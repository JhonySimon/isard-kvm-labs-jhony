### Destroy cluster

# (als dos nodes)
# pcs cluster destroy
# passwd hacluster

# (a un node)
# pcs host auth cluster1 cluster2 -u hacluster
# pcs cluster setup storage cluster1 cluster2
# pcs cluster start --all

# (a cada node)
# pcs cluster enable

ROOT_PWD=P@55W0RD

# CLUSTER CAN START WITH ONLY ONE NODE
#pcs property set no-quorum-policy=ignore
# LET ONLY ONE NODE START SERVICES WITHOUTH WAITING FOR THE OTHER ONE
#pcs cluster stop --all
#pcs quorum update wait_for_all=0
#pcs cluster start --all


# CLUSTER CAN START WITH ONLY ONE NODE
#pcs property set no-quorum-policy=ignore
# LET ONLY ONE NODE START SERVICES WITHOUTH WAITING FOR THE OTHER ONE
#pcs cluster stop --all
#pcs quorum update wait_for_all=0
#pcs cluster start --all

# DON'T LET CLUSTER ACTUATE WHILE WE CONFIGURE IT
#pcs property set maintenance-mode=true


## Disable cluster3 in drbd:
## drbdadm del-peer storage:cluster3
## drbdadm primary storage --force

# FENCING. simulate two ipmi
pcs stonith create stonith1 fence_virsh ip=192.168.122.1 username=root password=$ROOT_PWD ssh=true
pcs stonith create stonith2 fence_virsh ip=192.168.122.1 username=root password=$ROOT_PWD ssh=true
pcs constraint location stonith1 avoids cluster1-cs=INFINITY
pcs constraint location stonith2 avoids cluster2-cs=INFINITY

###################################3
###########DIRECT GFS2 ON DRBD
###################################
pcs resource create drbd ocf:linbit:drbd drbd_resource=storage op monitor interval=10s
pcs resource promotable drbd \
         promoted-max=2 promoted-node-max=1 clone-max=2 clone-node-max=1 \
         notify=true

pcs resource create dlm --group locking ocf:pacemaker:controld op monitor interval=10s on-fail=fence
pcs resource clone locking interleave=true

yes | mkfs.gfs2 -p lock_dlm -j 2 -J 256 -t storage:data /dev/drbd1000

pcs resource create filesystem Filesystem \
device=/dev/drbd1000 directory=/mnt \
fstype=gfs2 "options=defaults,noatime,nodiratime,noquota" op monitor interval=10s 

pcs resource clone filesystem clone-max=8 clone-node-max=1 \
        on-fail=fence interleave=true
        
pcs constraint order promote drbd-clone then locking-clone        
pcs constraint colocation add locking-clone with master drbd-clone

pcs constraint order locking-clone then filesystem-clone
pcs constraint colocation add filesystem-clone with locking-clone



pcs property set no-quorum-policy=freeze
pcs property set no-quorum-policy=ignore
pcs property set startup-fencing=false
pcs property set stonith-enabled=true

# Libvirt
apt install qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils libguestfs-tools genisoimage virtinst libosinfo-bin -y
systemctl disable --now libvirtd

pcs resource create libvirt systemd:libvirtd
pcs resource clone libvirt clone-max=2 clone-node-max=2 notify=true \
      on-fail=fence interleave=true 
pcs constraint order start filesystem-clone then libvirt-clone
pcs constraint colocation add libvirt-clone with filesystem-clone

## Rsync an image, for example debian10.qcow2 from host
qemu-img create -f qcow2 -F qcow2 -b debian10.qcow2 vm1.qcow2
qemu-img create -f qcow2 -F qcow2 -b debian10.qcow2 vm2.qcow2
qemu-img create -f qcow2 -F qcow2 -b debian10.qcow2 vm3.qcow2
qemu-img create -f qcow2 -F qcow2 -b debian10.qcow2 vm4.qcow2

pcs resource create vm1 VirtualDomain hypervisor="qemu:///system" \
   config="/opt/isard/vm1.xml" migration_transport=ssh \
   migration_network_suffix=.storage \
   op start timeout="120s" op stop timeout="120s" op monitor  timeout="30" interval="10" \
   meta allow-migrate="true" priority="100" \
   op migrate_from interval="0" timeout="120s" op migrate_to interval="0" timeout="120"

pcs constraint order start libvirt-clone then vm1 

pcs resource create vm2 VirtualDomain hypervisor="qemu:///system" \
   config="/opt/isard/vm2.xml" migration_transport=ssh \
   migration_network_suffix=.storage \
   op start timeout="120s" op stop timeout="120s" op monitor  timeout="30" interval="10" \
   meta allow-migrate="true" priority="100" \
   op migrate_from interval="0" timeout="120s" op migrate_to interval="0" timeout="120"

pcs constraint order start libvirt-clone then vm2

pcs resource create vm3 VirtualDomain hypervisor="qemu:///system" \
   config="/opt/isard/vm3.xml" migration_transport=ssh \
   migration_network_suffix=.storage \
   op start timeout="120s" op stop timeout="120s" op monitor  timeout="30" interval="10" \
   meta allow-migrate="true" priority="100" \
   op migrate_from interval="0" timeout="120s" op migrate_to interval="0" timeout="120"

pcs constraint order start libvirt-clone then vm3 

pcs resource create vm4 VirtualDomain hypervisor="qemu:///system" \
   config="/opt/isard/vm4.xml" migration_transport=ssh \
   migration_network_suffix=.storage \
   op start timeout="120s" op stop timeout="120s" op monitor  timeout="30" interval="10" \
   meta allow-migrate="true" priority="100" \
   op migrate_from interval="0" timeout="120s" op migrate_to interval="0" timeout="120"

pcs constraint order start libvirt-clone then vm4
#############################3
##################################



#### modify /etc/lvm/lvm.conf
filter = ["a|drbd.*|", "r|.*|"]

# in global config
   locking_type = 1
   use_lvmlockd = 1
   use_lvmetad = 1



#systemctl disable lvmlockd

# CLUSTER CAN START WITH ONLY ONE NODE
#pcs property set no-quorum-policy=ignore
# LET ONLY ONE NODE START SERVICES WITHOUTH WAITING FOR THE OTHER ONE
pcs cluster stop --all
pcs quorum update wait_for_all=1
pcs cluster start --all


# CLUSTER CAN START WITH ONLY ONE NODE
pcs property set no-quorum-policy=freeze
# LET ONLY ONE NODE START SERVICES WITHOUTH WAITING FOR THE OTHER ONE
#pcs cluster stop --all
#pcs quorum update wait_for_all=0
#pcs cluster start --all

# DON'T LET CLUSTER ACTUATE WHILE WE CONFIGURE IT
#pcs property set maintenance-mode=true

# FENCING. simulate two ipmi
pcs stonith create stonith1 fence_virsh ip=192.168.122.1 username=root password=$ROOT_PWD ssh=true
pcs stonith create stonith2 fence_virsh ip=192.168.122.1 username=root password=$ROOT_PWD ssh=true
## AVOID SHOOT IN THE HEAD.
pcs constraint location stonith1 avoids cluster1-cs=INFINITY
pcs constraint location stonith2 avoids cluster2-cs=INFINITY
## You should check that the fencind device is started before continuing!

pcs property set stonith-enabled=true
pcs property set maintenance-mode=false
## You should check that the fencind device is started before continuing!
pcs property set maintenance-mode=true

# LETS DISABLE MANTEINANCE
pcs property set maintenance-mode=false

pcs resource create storage ocf:linbit:drbd drbd_resource=storage op monitor interval=10s
pcs resource promotable storage \
         promoted-max=2 promoted-node-max=1 clone-max=2 clone-node-max=1 \
         notify=true

pcs resource create dlm --group locking ocf:pacemaker:controld op monitor interval=30s on-fail=fence
pcs resource clone locking interleave=true
pcs resource create lvmlockd --group locking ocf:heartbeat:lvmlockd op monitor interval=30s on-fail=fence

pcs constraint order promote storage-clone then start locking-clone
pcs constraint colocation add started locking-clone with master storage-clone



########### Aquí está el ejemplode red hat

#### create lvm cluster
#cluster1
vgcreate --shared shared_vg1 /dev/drbd0
#cluster2
vgchange --lock-start shared_vg1
lvcreate --activate sy -l 100%FREE -n shared_lv1 shared_vg1

pcs resource create sharedlv1 --group shared_vg1 ocf:heartbeat:LVM-activate lvname=shared_lv1 vgname=shared_vg1 activation_mode=shared vg_access_mode=lvmlockd
pcs resource clone shared_vg1 interleave=true
pcs constraint order start locking-clone then shared_vg1-clone
pcs constraint colocation add shared_vg1-clone with locking-clone

yes | mkfs.gfs2 -p lock_dlm -j 2 -J 128 -t storage:data /dev/shared_vg1/shared_lv1
pcs resource create sharedfs1 --group shared_vg1 ocf:heartbeat:Filesystem device="/dev/shared_vg1/shared_lv1" directory="/mnt/gfs1" fstype="gfs2" options=noatime op monitor interval=10s on-fail=fence

############## Más abajo está las configs que probamos

#### create lvm cluster
#cluster1
vgcreate --shared storage /dev/drbd0
#cluster2
vgchange --lock-start storage
lvcreate --activate sy -l 100%FREE -n data storage

pcs resource create data --group storage ocf:heartbeat:LVM-activate lvname=data vgname=storage activation_mode=shared vg_access_mode=lvmlockd
pcs resource clone storage interleave=true
pcs constraint order start locking-clone then storage-clone
pcs constraint colocation add storage-clone with locking-clone

yes | mkfs.gfs2 -p lock_dlm -j 2 -J 128 -t storage:data /dev/shared_vg1/shared_lv1
pcs resource create sharedfs1 --group shared_vg1 ocf:heartbeat:Filesystem device="/dev/shared_vg1/shared_lv1" directory="/mnt/gfs1" fstype="gfs2" options=noatime op monitor interval=10s on-fail=fence











pcs resource create storage ocf:linbit:drbd drbd_resource=storage op monitor interval=10s
pcs resource promotable storage master-max=2 master-node-max=1 clone-max=2 clone-node-max=1 notify=true

pcs resource create storage ocf:linbit:drbd drbd_resource=r0 op monitor interval=60s master master-max=2 master-node-max=1 clone-node-max=1 clone-max=2 notify=true op start interval=0s timeout=240 promote interval=0s timeout=130 monitor interval=150s role=Master monitor interval=155s role=Slave

pcs resource create dlm --group locking ocf:pacemaker:controld op monitor interval=30s on-fail=fence
pcs resource clone locking interleave=true
pcs resource create lvmlockd --group locking ocf:heartbeat:lvmlockd op monitor interval=30s on-fail=fence



#pcs resource create dlm ocf:pacemaker:controld op monitor interval=30s on-fail=fence
#pcs resource clone dlm interleave=true
#pcs resource create lvmlockd ocf:heartbeat:lvmlockd op monitor interval=30s on-fail=fence
#pcs resource clone lvmlockd interleave=true


vgcreate --shared storage /dev/drbd0
vgchange --lock-start storage
lvcreate --activate sy -l100%FREE -n data storage
yes | mkfs.gfs2 -p lock_dlm -j 2 -J 128 -t storage:data /dev/storage/data

pcs resource create shared-storage ocf:heartbeat:LVM-activate lvname=data vgname=storage activation_mode=shared vg_access_mode=lvmlockd
pcs resource clone shared-storage interleave=true






pcs constraint colocation add locking-clone with master storage-clone id=c1
pcs constraint order promote storage-clone then locking-clone id=o1

pcs resource create shared-data  ocf:heartbeat:LVM-activate lvname=data vgname=storage activation_mode=shared vg_access_mode=lvmlockd
pcs resource clone shared-data interleave=true

pcs constraint colocation add shared-data-clone with locking-clone id=c2
pcs constraint order locking-clone then shared-data-clone id=o2
#########################
#########################
### POR ALGUN MOTIVO FALLA AQUÍ Y NO ESPERA AL MASTER PARA ARRANCAR EL RESTO

pcs constraint colocation add master storage-clone with locking-clone 
pcs constraint order promote storage-clone then locking-clone

pcs resource create shared-data  ocf:heartbeat:LVM-activate lvname=data vgname=storage activation_mode=shared vg_access_mode=lvmlockd
pcs resource clone shared-data interleave=true

pcs constraint order start locking-clone then shared-storage-clone
pcs constraint colocation add shared-storage-clone with locking-clone


pcs constraint colocation add locking-clone with master storage-clone id=c1
pcs constraint order promote storage-clone then locking-clone id=o1



pcs constraint colocation add locking-clone with delay-clone id=c2
pcs constraint order start delay-clone then locking-clone id=o2


pcs 

pcs constraint order start locking-clone then shared-storage-clone
pcs constraint colocation add shared-storage-clone with locking-clone

###################
####################


pvcreate /dev/drbd0
vgcreate --shared storage /dev/drbd0
lvcreate --activate sy -l100%FREE -n data storage

#pcs resource update dlm on-fail=ignore
#pcs resource delete shared_lv_data-clone
#pcs property set maintenance-mode=true
#pcs property set stonith-enabled=false
################# GFS2









pcs constraint colocation add master storage-clone with locking-clone id=c1
pcs constraint order promote storage-clone then locking-clone kind=Serialize id=o1


pcs constraint colocation add shared-storage-clone with locking-clone id=o2
pcs constraint order start locking-clone then shared-storage-clone kind=Serialize id=c2


pcs constraint order \
    set storage-clone action=promote \
    set locking-clone action=start \
    set shared-storage-clone action=start \
    require-all=true sequential=true \
    setoptions kind=Mandatory id=serveis




pcs resource create storage-fs Filesystem \
        device="/dev/drbd/by-res/storage/0" directory="/mnt" \
        fstype="gfs2" "options=defaults,noatime,nodiratime,noquota" op monitor interval=10s

pcs resource clone storage-fs clone-max=2 clone-node-max=1 \
        on-fail=fence interleave=true 


pcs cluster enable



pcs resource clone storage-fs clone-max=2 clone-node-max=1 \
        on-fail=fence interleave=true
pcs constraint order start linstor-satellite-clone then dlm-clone
pcs constraint order start dlm-clone then start storage-fs-clone
pcs constraint colocation add dlm-clone with linstor-satellite-clone
pcs constraint colocation add storage-fs-clone with dlm-clone