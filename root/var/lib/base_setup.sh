cd /root
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
systemctl restart sshd
hostnumber=`cat /var/lib/hostnumber`

servername="cluster$hostnumber"

raid_device=/dev/md0
raid_dev1=/dev/vdb
raid_dev2=/dev/vdc

###################
#servername="cluster1"
echo "$servername" > /etc/hostname
sysctl -w kernel.hostname=$servername

echo "" >> /etc/hosts
echo "172.31.0.101 cluster1" >> /etc/hosts
echo "172.31.1.101 cluster1-drbd cluster1.storage" >> /etc/hosts
echo "172.31.2.101 cluster1-backup" >> /etc/hosts
echo "172.31.3.101 cluster1-cs" >> /etc/hosts
echo "172.31.4.101 cluster1-stonith" >> /etc/hosts
echo "172.31.4.201 cluster1-ipmi" >> /etc/hosts

echo "" >> /etc/hosts
echo "172.31.0.102 cluster2" >> /etc/hosts
echo "172.31.1.102 cluster2-drbd cluster2.storage" >> /etc/hosts
echo "172.31.2.102 cluster2-backup" >> /etc/hosts
echo "172.31.3.102 cluster2-cs" >> /etc/hosts
echo "172.31.4.102 cluster2-stonith" >> /etc/hosts
echo "172.31.4.202 cluster2-ipmi" >> /etc/hosts

echo "" >> /etc/hosts
echo "172.31.0.103 cluster3" >> /etc/hosts
echo "172.31.1.103 cluster3-drbd cluster3.storage" >> /etc/hosts
echo "172.31.2.103 cluster3-backup" >> /etc/hosts
echo "172.31.3.103 cluster3-cs" >> /etc/hosts
echo "172.31.4.103 cluster3-stonith" >> /etc/hosts
echo "172.31.4.203 cluster3-ipmi" >> /etc/hosts

apt install -y network-manager
systemctl enable --now NetworkManager

nmcli --fields UUID,TIMESTAMP-REAL con show | awk '{print $1}' | while read line; do nmcli con delete uuid $line; done
#nmcli --fields UUID,TIMESTAMP-REAL con show |  awk "{print $hostnumber}" | xargs -r -n1 nmcli con delete uuid
#nmcli --fields UUID,TIMESTAMP-REAL con show | grep never |  awk '{print $1}' | while read line; do nmcli con delete uuid  $line;    done

echo "" > /etc/network/interfaces

host=10$hostnumber

####################3
#host=101

if_out_name="enp1s0"
if_out_newname="out"
ip_out="192.168.122."$host
mask_out="24"
gw_out="192.168.122.1"
dns_out="192.168.122.1"

## DRBD intra cluster sync
## ISOLATED 10G
## Ex: cluster1-drbd
if_drbd="enp2s0"
net_drbd="172.31.1"

## Used to validate host intra-cluster (drbd/pacemaker ssh-copy-id ...)
## Ex: cluster1
net_cluster="172.31.0"
cluster_vlan_name="cluster"
cluster_vlan_id=100

## Backup
## backups
## Ex: cluster1-backup
net_backup="172.31.2"
backup_vlan_name="backup"
backup_vlan_id=102

## Pacemaker: Corosync
## No other traffic should be here! Critical
## Ex: cluster1-cs
net_corosync="172.31.3"
corosync_vlan_name="corosync"
corosync_vlan_id=103

## Pacemaker: Stonith
## No other traffic should be here! Critical
## Ex: cluster1-stonith
net_stonith="172.31.4"
stonith_vlan_name="stonith"
stonith_vlan_id=104

############################
##  END CONFIG PARAMETERS ##
############################

# SET DRBD INTERFACE
MAC=$(cat /sys/class/net/$if_drbd/address)
echo "SUBSYSTEM==\"net\", ACTION==\"add\", DRIVERS==\"?*\", ATTR{address}==\"$MAC\", ATTR{mtu}=\"9000\", KERNEL==\"enp*\", NAME=\"drbd\"" >> /etc/udev/rules.d/70-persistent-net.rules

ip link set $if_drbd down
ip link set $if_drbd name drbd
ip link set drbd mtu 9000
ip link set drbd up

nmcli con add con-name drbd ifname drbd type ethernet ip4 $net_drbd.$host/24 802-3-ethernet.mtu 9000  connection.autoconnect yes


## SET OUT: External services network
MAC=$(cat /sys/class/net/$if_out_name/address)
echo "SUBSYSTEM==\"net\", ACTION==\"add\", DRIVERS==\"?*\", ATTR{address}==\"$MAC\", KERNEL==\"enp*\", NAME=\"$if_out_newname\"" >> /etc/udev/rules.d/70-persistent-net.rules

ip link set $if_out_name down
ip link set $if_out_name name $if_out_newname
ip link set $if_out_newname up

# doesn't work
#nmcli con add con-name $if_out_newname ifname $if_out_newname type ethernet ipv4.addresses "$ip_out/$mask_out" ipv4.gateway "$gw_out" ipv4.dns "$dns_out" connection.autoconnect yes

nmcli con add con-name $if_out_newname ifname $if_out_newname type ethernet
nmcli con mod $if_out_newname ipv4.method manual ipv4.addresses "$ip_out/$mask_out" ipv4.gateway "$gw_out" ipv4.dns "$dns_out"
nmcli con mod $if_out_newname ipv6.method ignore
nmcli con up $if_out_newname

# SET CLUSTER VLAN INTERFACE OVER OUT
nmcli con add type vlan con-name $cluster_vlan_name.$cluster_vlan_id ifname $cluster_vlan_name.$cluster_vlan_id dev out id $cluster_vlan_id ipv4.method manual ipv4.addresses $net_cluster.$host/24 connection.autoconnect yes

# SET BACKUP VLAN INTERFACE OVER OUT
nmcli con add type vlan con-name $backup_vlan_name.$backup_vlan_id ifname $backup_vlan_name.$backup_vlan_id dev out id $backup_vlan_id ipv4.method manual ipv4.addresses $net_backup.$host/24 connection.autoconnect yes

# SET COROSYNC VLAN INTERFACE OVER OUT
nmcli con add type vlan con-name $corosync_vlan_name.$corosync_vlan_id ifname $corosync_vlan_name.$corosync_vlan_id dev out id $corosync_vlan_id ipv4.method manual ipv4.addresses $net_corosync.$host/24 connection.autoconnect yes

# SET STONITH VLAN INTERFACE OVER OUT
nmcli con add type vlan con-name $stonith_vlan_name.$stonith_vlan_id ifname $stonith_vlan_name.$stonith_vlan_id dev out id $stonith_vlan_id ipv4.method manual ipv4.addresses $net_stonith.$host/24 connection.autoconnect yes

############## HOST KEY
ssh-keygen -b 2048 -t rsa -f /root/.ssh/id_rsa -q -N ""
apt install rsync -yes
############## DRBD 
apt install lvm2 drbd-utils -y

modprobe drbd

########### RAID
apt install mdadm -y
yes | mdadm --create --verbose $raid_device --level=1 --raid-devices=2 $raid_dev1 $raid_dev2

echo "HOMEHOST <system>" > /etc/mdadm/mdadm.conf
echo "MAILADDR root" >> /etc/mdadm/mdadm.conf
mdadm --detail --scan >> /etc/mdadm/mdadm.conf
update-initramfs -u

###### LVM lockd
apt install lvm2-lockd -y

###### SSH root
apt install sshpass -y
#echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
#systemctl restart sshd

echo "[global]" > /etc/linstor/linstor-client.conf
echo "controllers=cluster1-drbd,cluster2-drbd,cluster3-drbd,cluster4-drbd,cluster5-drbd,cluster6-drbd,cluster7-drbd,cluster8-drbd" >> /etc/linstor/linstor-client.conf
### 
echo "######## BASE CONFIGURATION FINISHED ##########"

